//
//  AddParticipantToGroupViewController.swift
//  Mesh App
//
//  Created by Student on 18.05.2016.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit
import MeshBroadcastFramework

class AddParticipantToGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var group: Group!
    //var conversationParticipants: [Participant]!
    var activeParticipants: [Participant]!
    var newParticipants = [Participant]() //Participants that are active but not in the group list
    var selectedNewParticipants = [Participant]()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    private let meshFramework = (UIApplication.sharedApplication().delegate as! AppDelegate).meshFramework
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)

        //Create the list of participants that are active but not included in the group
        for activeParticipant in self.activeParticipants {
            if (!self.group.participants.contains(activeParticipant)) {
                newParticipants.append(activeParticipant)
            }
        }
        
        //Background image for the table view
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.tableView.backgroundView = backgroundImageView
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setBackgroundImage()
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.setBackgroundImage()
    }
    
    func setBackgroundImage() {
        let app = UIApplication.sharedApplication()
        
        let backgroundImage: UIImage!
        
        if (app.statusBarOrientation.isLandscape) {
            // Image Code
            backgroundImage = UIImage(named: "background-landscape")
        }
        else {
            // Image Code
            backgroundImage = UIImage(named: "background")
        }
        
        let backgroundImageView = self.tableView.backgroundView as! UIImageView
        backgroundImageView.image = backgroundImage
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newParticipants.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectGroupParticipantCell") as! SelectGroupParticipantCell
        let participant = self.newParticipants[indexPath.row]
        
        cell.participantName.text = self.newParticipants[indexPath.row].meshPeer.displayName
        cell.avatarInitialView.backgroundColor = participant.color
        cell.avatarInitialView.textLabel.text = participant.initials
        cell.participant = participant
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SelectGroupParticipantCell {
            //Remove participant from the selected list
            self.selectedNewParticipants.removeAtIndex((self.selectedNewParticipants.indexOf(cell.participant))!)
        }
    }
    
    //MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedNewParticipants.append(self.newParticipants[indexPath.row])
    }
    
    @IBAction func addNewParticipant(sender: UIBarButtonItem) {
        if let index = DataManager.sharedInstance.groups.indexOf(self.group){
            let storedGroup = DataManager.sharedInstance.groups[index]
            for newParticipant in self.selectedNewParticipants {
                storedGroup.participants.append(newParticipant)
            }
            self.group.participants = storedGroup.participants
        }
        
        var meshPeers = [MeshPeer]()
        for participant in self.selectedNewParticipants {
            meshPeers.append(participant.meshPeer)
        }
        
        //Send an invitation to the peer for all unacknowledged groups
        let invitationDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Invitation, Constants.Message.Key.Payload: group]
        let messageData = NSKeyedArchiver.archivedDataWithRootObject(invitationDataDictionary)
        meshFramework.sendData(messageData, toPeers: meshPeers, conversationId: group.groupId)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}