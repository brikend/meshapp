//
//  AppDelegate.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit
import CoreData
import MeshBroadcastFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MeshBroadcastFrameworkDelegate {

    var window: UIWindow?
    let meshFramework = MeshBroadcastFramework()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //Start searching and advertising for other peers
        self.meshFramework.startMeshNetwork()
        self.meshFramework.delegate = self
        
        //Customize the appearance of the status bar
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //Post a notification when the app entered the background (e.g. home button pressed)
        NSNotificationCenter.defaultCenter().postNotificationName("applicationDidEnterBackgroundNotification", object: nil)
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        //Post a notification when the app will enter the foreground (e.g. re-opening the app)
        NSNotificationCenter.defaultCenter().postNotificationName("applicationWillEnterForegroundNotification", object: nil)
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "no.ntnu.Mesh_App" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Mesh_App", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //MARK: - MeshBroadcastFrameworkDelegate
    func messageReceived(message: Message) {
        //Extract the dataDictionary from the framework's message-object
        if let dataDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(message.payload)! as? NSDictionary,
            let messageType = dataDictionary[Constants.Message.Key.MessageType] as? String {
            //Handle different types of messages differently
            switch messageType {
            case Constants.Message.Type.Message:
                //Store the received message
                DataManager.sharedInstance.storeMessage(message)
                
                //Post a notification when the app receives a message from another peer in the mesh
                NSNotificationCenter.defaultCenter().postNotificationName("messageReceivedNotification", object: nil, userInfo: ["message": message])
                
            case Constants.Message.Type.Invitation:
                //Extract the group information
                if let group = dataDictionary[Constants.Message.Key.Payload] as? Group {
                    //Check if I'm already participating in this group
                    if let storedGroup = DataManager.sharedInstance.getGroupForId(group.groupId) {
                        //We are already participating in the group
                        
                        //Update the participants list
                        storedGroup.participants = group.participants
                        
                        //Set sender as acknowledged
                        DataManager.sharedInstance.setPeerAsAcknowledged(message.sender.meshPeerID, forGroup: storedGroup)
                        
                        //Send an acknowledgement message back to the sender
                        let acknowledgementDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Acknowledgement, Constants.Message.Key.Payload: group.groupId]
                        let messageData = NSKeyedArchiver.archivedDataWithRootObject(acknowledgementDataDictionary)
                        meshFramework.sendData(messageData, toPeers: [message.sender], conversationId: group.groupId)
                    }
                    else {
                        //We are not participating in the group
                        DataManager.sharedInstance.groups.append(group)
                        
                        //print("Group appended - Group count: \(DataManager.sharedInstance.groups.count)")
                        
                        //Set sender as acknowledged
                        DataManager.sharedInstance.setPeerAsAcknowledged(message.sender.meshPeerID, forGroup: group)
                        
                        //Send an acknowledgement message to all participants of the group
                        var meshPeers = [MeshPeer]()
                        for participant in group.participants {
                            if (self.meshFramework.meshPeer!.meshPeerID != participant.meshPeer.meshPeerID) {
                                meshPeers.append(participant.meshPeer)
                            }
                        }
                        
                        let acknowledgementDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Acknowledgement, Constants.Message.Key.Payload: group.groupId]
                        let messageData = NSKeyedArchiver.archivedDataWithRootObject(acknowledgementDataDictionary)
                        meshFramework.sendData(messageData, toPeers: meshPeers, conversationId: group.groupId)
                        
                        //Post a notification when the app is informed about being invited to a group by another peer
                        NSNotificationCenter.defaultCenter().postNotificationName("invitationReceivedNotification", object: nil, userInfo: ["message": message])
                    }
                }
                
            case Constants.Message.Type.Acknowledgement:
                //Extract the group information
                if let groupId = dataDictionary[Constants.Message.Key.Payload] as? String {
                    //Retrive the group from the DataManager
                    if let storedGroup = DataManager.sharedInstance.getGroupForId(groupId) {
                        //Get all of the MeshPeers in the group
                        var meshPeers = [MeshPeer]()
                        for participant in storedGroup.participants {
                            meshPeers.append(participant.meshPeer)
                        }
                        
                        //Check if the sender is in the participant list of the group
                        var isSenderInGroupsParticipants = false
                        
                        for meshPeer in meshPeers {
                            if (meshPeer.meshPeerID == message.sender.meshPeerID) {
                                isSenderInGroupsParticipants = true
                                break
                            }
                        }
                        
                        //Sender is not in the list of the groups participants - append him
                        if (!isSenderInGroupsParticipants) {
                            //It is not in the group - add the sender as a participant of the group
                            let participant = Participant(meshPeer: message.sender, infoText: "")
                            storedGroup.participants.append(participant)
                        }

                        //Set the sender acknowledged for the group
                        storedGroup.acknowledgedPeers.append(message.sender.meshPeerID)
                        
                        //Post a notification when the app is informed about an incoming acknowledgement from another peer
                        NSNotificationCenter.defaultCenter().postNotificationName("acknowledgementReceived", object: nil, userInfo: ["message": message])
                    }
                }
                
            default:
                return
            }
        }
    }
    
    func peerDiscovered(peer: MeshPeer) {
        //Find all unacknowledged peers in all groups
        if let unacknowledgedGroups = DataManager.sharedInstance.getUnacknowledgedGroupsForPeer(peer) {
            for unacknowledgedGroup in unacknowledgedGroups {
                //Send an invitation to the peer for all unacknowledged groups
                let invitationDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Invitation, Constants.Message.Key.Payload: unacknowledgedGroup]
                let messageData = NSKeyedArchiver.archivedDataWithRootObject(invitationDataDictionary)
                meshFramework.sendData(messageData, toPeers: [peer], conversationId: unacknowledgedGroup.groupId)
            }
        }
        
        //Post a notification when the app is informed about a newly discovered peer
        NSNotificationCenter.defaultCenter().postNotificationName("peerDiscoveredNotification", object: nil, userInfo: ["meshPeer": peer])        
    }
    
    func peerDropped(peer: MeshPeer) {
        //Post a notification when the app is informed about a dropped peer
        NSNotificationCenter.defaultCenter().postNotificationName("peerDroppedNotification", object: nil, userInfo: ["meshPeer": peer])
    }
}

