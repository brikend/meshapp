//
//  Constants.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 28/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import UIKit

//Define constant values that the application uses
struct Constants {
    struct Message {
        struct Key {
            static let MessageType = "MessageType"
            static let Payload = "Payload"
        }
        struct Type {
            static let Message = "Message"
            static let Invitation = "Invitation"
            static let Acknowledgement = "Acknowledgement"
        }
    }
    struct DataType {
            static let Image = "Image"
            static let Text = "Text"
    }
}