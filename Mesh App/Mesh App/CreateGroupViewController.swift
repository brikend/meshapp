//
//  CreateGroupViewController.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 09/05/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit
import MeshBroadcastFramework

class CreateGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    //Outlet-variables that are linked to the Storyboard
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var groupInitialView: AvatarInitialView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupStatusLabel: UILabel!
    @IBOutlet weak var groupNameTxtField: UITextField!
    @IBOutlet weak var groupStatusTxtField: UITextField!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    var participants: [Participant]!
    var selectedParticipants = [Participant]()
    private let meshFramework = (UIApplication.sharedApplication().delegate as! AppDelegate).meshFramework

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        
        self.tableView.backgroundColor = UIColor.clearColor()
        
        groupInitialView.textLabel.text = "?"
        groupInitialView.textLabel.font = UIFont.boldSystemFontOfSize(30.0)
        groupInitialView.textLabel.textColor = UIColor.lightGrayColor()
        groupInitialView.backgroundColor = UIColor.whiteColor()
        
        let path = UIBezierPath(arcCenter: CGPoint(x: groupInitialView.bounds.origin.x + groupInitialView.bounds.size.width/2, y: groupInitialView.bounds.origin.y + groupInitialView.bounds.size.height/2), radius: groupInitialView.bounds.size.width/2 + 1.0, startAngle: 0.0, endAngle: CGFloat(M_PI), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightMeshChatColor().CGColor
        shapeLayer.lineWidth = 2.0
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        shapeLayer.path = path.CGPath
        groupInitialView.layer.addSublayer(shapeLayer)
        
        //Hide the text field and add touchable actions on the groupName Label
        groupNameTxtField.delegate = self
        groupNameTxtField.hidden = true
        groupNameLabel.userInteractionEnabled = true
        let groupLabelSelector : Selector = #selector(CreateGroupViewController.groupLabelTapped)
        let groupNameTapGesture = UITapGestureRecognizer(target: self, action: groupLabelSelector)
        groupNameTapGesture.numberOfTapsRequired = 1
        groupNameLabel.addGestureRecognizer(groupNameTapGesture)
        groupNameTxtField.returnKeyType = UIReturnKeyType.Done
        
        //Hide the text field and add touchable actions on the groupStatus Label
        groupStatusTxtField.delegate = self
        groupStatusTxtField.hidden = true
        groupStatusLabel.userInteractionEnabled = true
        let groupStatusSelector : Selector = #selector(CreateGroupViewController.groupStatusLabelTapped)
        let groupStatusTapGesture = UITapGestureRecognizer(target: self, action: groupStatusSelector)
        groupStatusTapGesture.numberOfTapsRequired = 1
        groupStatusLabel.addGestureRecognizer(groupStatusTapGesture)
        groupStatusTxtField.returnKeyType = UIReturnKeyType.Done
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.setBackgroundImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var groupNameTouched = true
    
    func groupLabelTapped(){
        groupNameLabel.hidden = true
        groupNameTxtField.hidden = false
        if (groupNameLabel.text == "Group Name") {
            groupNameTxtField.text = ""
        } else {
            groupNameTxtField.text = groupNameLabel.text
        }
        groupNameTouched = true
        groupNameTxtField.becomeFirstResponder()
    }
    func groupStatusLabelTapped(){
        groupStatusLabel.hidden = true
        groupStatusTxtField.hidden = false
        if (groupStatusLabel.text == "\"What is this group about?\"") {
            groupStatusTxtField.text = ""
        } else {
            groupStatusTxtField.text = groupStatusLabel.text
        }
        groupNameTouched = false
        groupStatusTxtField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (groupNameTouched) {
            if(groupNameTxtField.text == "") {
                groupNameTxtField.text = "Group Name"
            }
            
            groupNameTxtField.hidden = true
            groupNameLabel.hidden = false
            groupNameLabel.text = groupNameTxtField.text
            groupInitialView.textLabel.text = Utilities.createInitials(groupNameTxtField.text!)
        } else {
            if(groupStatusTxtField.text == "") {
                groupStatusTxtField.text = "\"What is this group about?\""
            }
            groupStatusTxtField.hidden = true
            groupStatusLabel.hidden = false
            groupStatusLabel.text = groupStatusTxtField.text
        }
        return true
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.setBackgroundImage()
    }
    
    func setBackgroundImage() {
        let app = UIApplication.sharedApplication()
        
        let backgroundImage: UIImage!
        
        if (app.statusBarOrientation.isLandscape) {
            // Image Code
            backgroundImage = UIImage(named: "background-landscape")
        }
        else {
            // Image Code
            backgroundImage = UIImage(named: "background")
        }
        
        self.backgroundImageView.image = backgroundImage
    }
    
    @IBAction func createNewGroup(sender: UIBarButtonItem) {
        
        //Check if user has selected at least 2 participants to participate in the new group
        if (selectedParticipants.count < 2) {
            alertUser()
            return
        }
        
        let groupID = NSUUID().UUIDString
        
        //Create an array of all participants (selected and myself)
        var groupParticipants = self.selectedParticipants
        groupParticipants.insert(Participant(meshPeer: meshFramework.meshPeer!, infoText: ""), atIndex: 0)
        
        //Create a group and store it
        let group = Group(groupId: groupID, title: groupNameLabel.text!, infoText: groupStatusLabel.text!, participants: groupParticipants)
        DataManager.sharedInstance.groups.append(group)
        
        var meshPeers = [MeshPeer]()
        for participant in self.selectedParticipants {
            meshPeers.append(participant.meshPeer)
        }
        
        //Send an invitation to the peer for all unacknowledged groups
        let invitationDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Invitation, Constants.Message.Key.Payload: group]
        let messageData = NSKeyedArchiver.archivedDataWithRootObject(invitationDataDictionary)
        meshFramework.sendData(messageData, toPeers: meshPeers, conversationId: group.groupId)
                
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func alertUser() {
        let alert = UIAlertController(title: "Attention", message: "You must select at least 2 participants!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }

    @IBAction func cancel(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
         if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SelectGroupParticipantCell {
            //Remove participant from the selected list
            self.selectedParticipants.removeAtIndex(self.selectedParticipants.indexOf(cell.participant)!)
        }
    }
    
    //MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedParticipants.append(self.participants[indexPath.row])
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.participants.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectGroupParticipantCell") as! SelectGroupParticipantCell
        let participant = self.participants[indexPath.row]
        
        cell.participantName.text = self.participants[indexPath.row].meshPeer.displayName
        cell.avatarInitialView.backgroundColor = participant.color
        cell.avatarInitialView.textLabel.text = participant.initials
        cell.participant = participant
        
        return cell
    }
}