//
//  MeshAppMessage.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 28/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MeshBroadcastFramework

//Wraps the framework's Message-Object into a custom object that can we utilized
class MeshAppMessage: NSObject {
    let conversationId: String
    let senderId: String
    let senderDisplayName: String
    let messageType: String
    let payload: NSData
    let timeStamp: NSDate
    
    init(conversationId: String, senderId: String, senderDisplayName: String, messageType: String, payload: NSData, timeStamp: NSDate) {
        self.conversationId = conversationId
        self.senderId = senderId
        self.senderDisplayName = senderDisplayName
        self.messageType = messageType
        self.payload = payload
        self.timeStamp = timeStamp
    }
}