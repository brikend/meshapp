//
//  ChatRoom.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 22/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import UIKit

//Represents a conversation with multiple people
class Group: NSObject, NSCoding {
    let groupId: String
    let title: String
    var infoText: String?
    var participants: [Participant]
    var acknowledgedPeers = [String]()
    var color: UIColor?
    var initials: String?
    var activeParticipantCount: Int
    
    init(groupId: String, title: String, infoText: String?, participants: [Participant]) {
        self.groupId = groupId
        self.title = title
        self.infoText = infoText
        self.participants = participants
        
        self.color = UIColor.randomColor()
        self.initials = Utilities.createInitials(title)
        
        self.activeParticipantCount = participants.count
    }
    
    //MARK: - NSCoding
    required init(coder aDecoder: NSCoder) {
        self.groupId = aDecoder.decodeObjectForKey("groupId") as! String
        self.title = aDecoder.decodeObjectForKey("title") as! String
        self.infoText = aDecoder.decodeObjectForKey("infoText") as! String?
        self.participants = aDecoder.decodeObjectForKey("participants") as! [Participant]
        self.acknowledgedPeers = aDecoder.decodeObjectForKey("acknowledgedPeers") as! [String]
        self.color = aDecoder.decodeObjectForKey("color") as! UIColor?
        self.initials = aDecoder.decodeObjectForKey("initials") as! String?
        self.activeParticipantCount = aDecoder.decodeObjectForKey("activeParticipantCount") as! Int
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.groupId, forKey: "groupId")
        aCoder.encodeObject(self.title, forKey: "title")
        aCoder.encodeObject(self.infoText, forKey: "infoText")
        aCoder.encodeObject(self.participants, forKey: "participants")
        aCoder.encodeObject(self.acknowledgedPeers, forKey: "acknowledgedPeers")
        aCoder.encodeObject(self.color, forKey: "color")
        aCoder.encodeObject(self.initials, forKey: "initials")
        aCoder.encodeObject(self.activeParticipantCount, forKey: "activeParticipantCount")
    }
}