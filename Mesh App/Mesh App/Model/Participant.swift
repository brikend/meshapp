//
//  People.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 22/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MeshBroadcastFramework

//Represents a user that is participating in the mesh chat
class Participant: NSObject, NSCoding {
    var meshPeer: MeshPeer!
    var infoText: String?
    var color: UIColor?
    var initials: String?
    
    init(meshPeer: MeshPeer, infoText: String?) {
        super.init()
        
        self.meshPeer = meshPeer
        self.infoText = infoText
        
        //Create a random color
        self.color = UIColor.randomColor()
        self.initials = Utilities.createInitials(meshPeer.displayName)
    }
    
    //TODO - Replace participant comparison for loop with == operator
    override func isEqual(object: AnyObject?) -> Bool {
        if let object = object as? Participant {
            if (meshPeer.meshPeerID == object.meshPeer.meshPeerID) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    override var hash: Int {
        return meshPeer.meshPeerID.hashValue
    }
    
    //MARK: - NSCoding
    required init(coder aDecoder: NSCoder) {
        self.meshPeer = aDecoder.decodeObjectForKey("meshPeer") as! MeshPeer
        self.infoText = aDecoder.decodeObjectForKey("infoText") as! String?
        self.color = aDecoder.decodeObjectForKey("color") as! UIColor?
        self.initials = aDecoder.decodeObjectForKey("initials") as! String?
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.meshPeer, forKey: "meshPeer")
        aCoder.encodeObject(self.infoText, forKey: "infoText")
        aCoder.encodeObject(self.color, forKey: "color")
        aCoder.encodeObject(self.initials, forKey: "initials")
    }
}