//
//  DataManager.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 28/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MeshBroadcastFramework

//This class provides centralized access to resources related to groups and messages
class DataManager {
    //Dictionary of messages (value of the dictionary) that have been stored for a specific conversationId (key of the dictionary)
    var storedMessages = [String: [MeshAppMessage]]()
    
    //Dictionary that keeps track of unread and read conversations (the key of the dictionary is the conversationId, the value is a boolean value indicating whether or not this conversation has been read)
    var readConversations = [String: Bool]()
    
    //An array of groups
    var groups = [Group]()
    
    //The singleton instance of the DataManager
    static let sharedInstance = DataManager()
    
    //MARK: - Messages
    func storeMessage(message: Message) {
        //Extract the dataDictionary from the framkework's message-object
        if let dataDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(message.payload)! as? NSDictionary {
            //Convert the message to the custom MeshAppMessage
            let meshAppMessage = MeshAppMessage(conversationId: message.conversationId, senderId: message.sender.meshPeerID, senderDisplayName: message.sender.displayName, messageType: Constants.DataType.Text, payload: dataDictionary[Constants.Message.Key.Payload] as! NSData, timeStamp: message.timeStamp)
            
            //Store the message
            self.storeMessage(meshAppMessage)
        }
    }
    
    func storeMessage(message: MeshAppMessage) {
        //Check if we have already stored messages for this conversation
        if (DataManager.sharedInstance.storedMessages[message.conversationId] != nil) {
            //Append the message to the already existing array of stored messages
            var messageArray = DataManager.sharedInstance.storedMessages[message.conversationId]! as [MeshAppMessage]
            messageArray.append(message)
            DataManager.sharedInstance.storedMessages[message.conversationId] = messageArray
        }
            
        else {
            //There are no messages stored for this conversation - create a new entry in the dictionary and store the message
            var messageArray = [MeshAppMessage]()
            messageArray.append(message)
            DataManager.sharedInstance.storedMessages[message.conversationId] = messageArray
        }
        
        //Mark the conversation as unread
        self.readConversations[message.conversationId] = false
    }
    
    //MARK: - Groups
    func getGroupForId(groupId: String) -> Group? {
        //Iterate all of the groups
        for group in self.groups {
            //Check if their IDs match
            if group.groupId == groupId {
                return group
            }
        }
        
        return nil
    }
    
    func setPeerAsAcknowledged(peerId: String, forGroup group: Group) {
        //Check if the peer is already set to acknowledged for this group
        if !group.acknowledgedPeers.contains(peerId) {
            //It is not acknowledged - we have to set the peer as acknowledged
            group.acknowledgedPeers.append(peerId)
        }
    }
    
    func getUnacknowledgedGroupsForPeer(peer: MeshPeer) -> [Group]? {
        var unacknowledgedGroups = [Group]()
        
        //Iterate all of the groups
        for group in self.groups {
            //Get all unacknowledged peers for the group
            for participant in group.participants {
                if participant.meshPeer.meshPeerID == peer.meshPeerID {
                    //If the participant is not yet acknowledged, we can add this group to the array of unacknowledged ones
                    if (!group.acknowledgedPeers.contains(participant.meshPeer.meshPeerID)) {
                        unacknowledgedGroups.append(group)
                    }
                }
            }
        }
        
        return unacknowledgedGroups.count > 0 ? unacknowledgedGroups : nil
    }
}