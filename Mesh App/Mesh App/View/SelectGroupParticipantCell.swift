//
//  SelectGroupParticipantCell.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 09/05/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit

//Custom UITableViewCell that is used for selecting people when creating a new group or adding them to an already existing group
class SelectGroupParticipantCell: UITableViewCell {
    //Outlet-variables that are linked to the Storyboard
    @IBOutlet weak var avatarInitialView: AvatarInitialView!
    @IBOutlet weak var participantName: UILabel!
    
    //The participant that is represented by this cell
    var participant: Participant!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Customize the appearance when the cell is instantiated from the Storyboard
        self.backgroundColor = UIColor.clearColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if (selected) {
            //Add an checkmark accessory to selected cells
            self.accessoryType = .Checkmark
        }
        else {
            //Unselected cells don't get any accessory
            self.accessoryType = .None
        }
    }
}