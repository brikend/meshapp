//
//  OverViewCell.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 22/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit

//Custom UITableViewCell that is used for displaying potential conversation partners (can either be groups or people) within the initial ViewController's TableView
class OverViewCell: UITableViewCell {
    //Outlet-variables that are linked to the Storyboard
    @IBOutlet weak var avatarImageView: AvatarInitialView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var detailInformation: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nrParticipants: AvatarInitialView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height/2
//        self.avatarImageView.layer.borderWidth = 3.0
//        self.avatarImageView.layer.borderColor = UIColor.meshChatMainColor().CGColor
//        self.avatarImageView.layer.masksToBounds = true
        
        //Customize the appearance when the cell is instantiated from the Storyboard
        nrParticipants.textLabel.font = UIFont.boldSystemFontOfSize(14.0)
        nrParticipants.textLabel.textColor = UIColor.whiteColor()
        nrParticipants.backgroundColor = UIColor.lightMeshChatColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}