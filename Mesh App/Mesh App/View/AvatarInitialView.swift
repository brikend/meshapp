//
//  AvatarInitialView.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 15/05/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit

//Custom UIView that is used to either display a user's avatar, including a color and his initials, or to display the number of active participants in a group
class AvatarInitialView: UIView {
    var textLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor.blueColor()
        self.layer.cornerRadius = frame.size.width/2
        
        //Initialize the label and customize its appearance
        self.textLabel = UILabel(frame: self.bounds)
        textLabel.backgroundColor = UIColor.clearColor()
        textLabel.textAlignment = .Center
        textLabel.font = UIFont.systemFontOfSize(14.0)
        textLabel.textColor = UIColor.whiteColor()
        textLabel.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        self.addSubview(textLabel)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
}