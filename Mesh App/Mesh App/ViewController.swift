//
//  ViewController.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit
import MeshBroadcastFramework

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //Outlet-variables that are linked to the Storyboard
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var addGroupButton: UIBarButtonItem!

    //The displayedArray represents the data source of the ViewController's TableView and can either contain Participants or Groups depending on which state the segmentedControl is
    var displayedArray = []
    
    //Group and Participant arrays that are being interacted with
    var groups = [Group]()
    var participants = [Participant]()
    
    //Get the meshframework-instance from the AppDelegate
    private let meshFramework = (UIApplication.sharedApplication().delegate as! AppDelegate).meshFramework
    
    //Handle the "nearbyPeopleGroup" specifically and different from other groups
    private var nearbyPeopleGroup: Group!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Customize the appearance of the TableView
        self.tableView.contentInset = UIEdgeInsetsMake(self.toolbar.frame.size.height + 20.0, 0.0, self.toolbar.frame.size.height + 20.0, 0.0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.toolbar.frame.size.height + 20.0, 0.0, self.toolbar.frame.size.height + 20.0, 0.0)
        
        //Rounded segmented control
        self.segmentedControl.layer.cornerRadius = 15.0;
        self.segmentedControl.layer.borderWidth = 1.0;
        self.segmentedControl.layer.masksToBounds = true;
        self.segmentedControl.layer.borderColor = self.segmentedControl.tintColor.CGColor
        //Change height of segmented control
        self.segmentedControl.frame = CGRect(x: segmentedControl.frame.origin.x, y: segmentedControl.frame.origin.y, width: segmentedControl.frame.size.width, height: 25)
        
        self.toolbar.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.0)
        
        //Register functions that should be called for incoming (mesh-related) notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.messageReceived(_:)), name: "messageReceivedNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.messageSent(_:)), name: "messageSentNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.addPeer(_:)), name: "peerDiscoveredNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.removePeer(_:)), name: "peerDroppedNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.invitationReceived), name: "invitationReceivedNotification", object: nil)
        
        //Initialize the nearby people chat room
        self.nearbyPeopleGroup = Group(groupId: "NEARBY-PEOPLE", title: "People Nearby", infoText: "Chat with people around you", participants: self.participants)
        self.groups.append(nearbyPeopleGroup)
        
        //Initially, set the groups to be the displayed data source
        self.displayedArray = groups
        
        //Set the title
        self.title = "Groups Nearby"
        
        //Fix for Apple bug
        self.navigationController!.view.backgroundColor = UIColor.whiteColor()
        
        //Add the "mesh" to the background
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.tableView.backgroundView = backgroundImageView
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //Fix bug where selected cells are still highlighted after going back to this view
        if let indexPathForSelectedCell = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(indexPathForSelectedCell, animated: true)
        }
        
        //Adjust the background image
        self.setBackgroundImage()
        
        //Refresh the active groups
        self.refreshActiveGroups()
        
        //Set the correct data source depending on which state the segmentedControl is at
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            self.displayedArray = self.groups
        }
        else {
            self.displayedArray = self.participants
        }
        
        //Reload the TableView's data
        self.tableView.reloadData()
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        //Adjust the background image when the orientation changes
        self.setBackgroundImage()
    }
    
    func setBackgroundImage() {
        let app = UIApplication.sharedApplication()
        
        let backgroundImage: UIImage!
        
        //Set a different image for different orientations
        if (app.statusBarOrientation.isLandscape) {
            backgroundImage = UIImage(named: "background-landscape")
        }
        else {
            backgroundImage = UIImage(named: "background")
        }
        
        //Apply new image to the TableView
        let backgroundImageView = self.tableView.backgroundView as! UIImageView
        backgroundImageView.image = backgroundImage
    }
    
    @IBAction func segmentedControlClicked(sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            //Display and enable the "+"-Button when we're in the "Group"-Tab
            self.addGroupButton.enabled = true
            self.addGroupButton.tintColor = UIColor.whiteColor()
            
            //Refresh the active groups
            self.refreshActiveGroups()
            
            //Set the correct data source
            self.displayedArray = self.groups
            
            //Set the title
            self.title = "Groups Nearby"
        }
        else {
            //Set the correct data source
            self.displayedArray = self.participants
            
            //Set the title
            self.title = "People Nearby"
            
            //Hide and disable the "+"-Button in the navigation bar when we're in the "People"-Tab
            self.addGroupButton.enabled = false
            self.addGroupButton.tintColor = UIColor.clearColor()
        }
        
        //Reload the TableView's data
        self.tableView.reloadData()
    }
    
    private func isGroupActive(group: Group) -> Bool {
        //Iterate all of the given group's participants
        for participant in group.participants {
            //Iterate all of the active participants of our data source
            for activeParticipant in self.participants {
                //Check if the participants' IDs match
                if (participant.meshPeer.meshPeerID == activeParticipant.meshPeer.meshPeerID) {
                    //We have at least one participant - add the group to the displayed group array because the group is active
                    return true
                }
            }
        }

        return false
    }
    
    private func getActiveGroups(groups: [Group]) -> [Group]? {
        var activeGroups = [Group]()

        //Append each group from the data source that is active to the array
        for group in groups {
            if (self.isGroupActive(group)) {
                activeGroups.append(group)
            }
        }
        
        return activeGroups.count > 0 ? activeGroups : nil
    }
    
    private func refreshActiveGroups() {
        //Clear the array
        self.groups.removeAll()
        
        //Always append the special nearbyPeopleGroup
        self.groups.append(self.nearbyPeopleGroup)
        
        //Also append any other active group
        if let activeGroups = self.getActiveGroups(DataManager.sharedInstance.groups) {
            self.groups += activeGroups
        }
    }
    
    private func refreshActiveGroupsAndReloadUI() {
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            //Group Tab
            
            //Store the current group array
            let oldGroups = self.groups
            
            //Refresh rooms
            self.refreshActiveGroups()
            
            //Set the correct Datasource
            self.displayedArray = self.groups
            
            //Update the UI
            
            //Get only the newly addedGroups and init the indexPaths
            var indexPaths = [NSIndexPath]()
            
            outerLoop: for (index, group) in self.groups.enumerate() {
                for oldGroup in oldGroups {
                    if (group.groupId == oldGroup.groupId) {
                        continue outerLoop
                    }
                }
                
                indexPaths.append(NSIndexPath(forRow: index, inSection: 0))
            }
            
            self.tableView.beginUpdates()
            
            //Insert a new row at the bottom of the currently displayed list
            self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            
            self.tableView.endUpdates()
        }
        else {
            //People Tab
            //Just refresh the data (not the UI)
            self.refreshActiveGroups()
        }
    }
    
    private func getGroupsForParticipant(participant: Participant) -> [Group] {
        var participantGroups = [Group]()
        
        //Always append the special nearbyPeopleGroup
        participantGroups.append(self.nearbyPeopleGroup)
        
        //Iterate all groups stored in the DataManager
        for group in DataManager.sharedInstance.groups {
            //Check if the participant is contained within the group and append him to the array if he is
            if (group.participants.contains(participant)) {
                participantGroups.append(group)
            }
        }
        
        return participantGroups
    }
    
    private func getFormattedDate(timeStamp: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .NoStyle
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter.stringFromDate(timeStamp)
    }
    
    //MARK: - Mesh Notification functions
    func addPeer(notification: NSNotification) {
        //Make sure that we're operating on the UI-thread
        dispatch_async(dispatch_get_main_queue(), {
            //print("ViewController peerDiscovered")
            
            //Extract data from the notification
            if let userInfo = notification.userInfo {
                //Extract the meshPeer-Object that's contained in the userInfo of the notification
                let meshPeer = userInfo["meshPeer"] as! MeshPeer
                
                //Add a new Participant-Object into our list
                let participant = Participant(meshPeer: meshPeer, infoText: "")
                self.participants.append(participant)
                
                //After discovering a new peer, increase the count of active participants of each group that this participant is in
                let participantGroups = self.getGroupsForParticipant(participant)
                for participantGroup in participantGroups {
                    participantGroup.activeParticipantCount += 1
                }
                
                //Refresh the nearby people group's participants
                self.nearbyPeopleGroup.participants = self.participants
                
                //Check if we're in the 'People'-Tab
                if (self.segmentedControl.selectedSegmentIndex == 1) {
                    //Set the correct Datasource
                    self.displayedArray = self.participants
                    
                    //Update the UI
                    self.tableView.beginUpdates()
                    
                    //Insert a new row at the bottom of the currently displayed list
                    self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self.participants.count - 1, inSection: 0)], withRowAnimation: .Automatic)
                    
                    self.tableView.endUpdates()
                    
                }
                else if (self.segmentedControl.selectedSegmentIndex == 0) {
                    self.refreshActiveGroupsAndReloadUI()
                    
                    //Refresh any cells that were affected by the participant (the active participant count will have changed)
                    self.refreshCellsForGroups(participantGroups)
                }
            }
        })
    }
    
    private func refreshCellsForGroups(affectedGroups: [Group]) {
        //Get indexPaths of all the groups where the activeParticipantCount changed
        var indexPaths = [NSIndexPath]()
        
        //Append the people nearby group so that it will be refreshed each time the function gets called
        //indexPaths.append(NSIndexPath(forRow: 0, inSection: 0))
        
        outerLoop: for (index, group) in self.groups.enumerate() {
            for affectedGroup in affectedGroups {
                if (affectedGroup.groupId == group.groupId) {
                    indexPaths.append(NSIndexPath(forRow: index, inSection: 0))
                    continue outerLoop
                }
            }
        }
        
        //Reload the affected cells
        self.tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
    }
    
    func removePeer(notification: NSNotification) {
        //Make sure that we're operating on the UI-thread
        dispatch_async(dispatch_get_main_queue(), {
            //Extract data from the notification
            if let userInfo = notification.userInfo {
                //Extract the meshPeer-Object that's contained in the userInfo of the notification
                let meshPeer = userInfo["meshPeer"] as! MeshPeer
                
                //Search for the removed peer in our list
                var participantToRemove: Participant?
                for participant in self.participants {
                    if (participant.meshPeer.meshPeerID == meshPeer.meshPeerID) {
                        //If the IDs match, remove the object from the list
                        participantToRemove = participant
                        break
                    }
                }
                
                if (participantToRemove != nil) {
                    //After losing an existing peer, decrease the count of active participants of each group that this participant is in
                    let participantGroups = self.getGroupsForParticipant(participantToRemove!)
                    for participantGroup in participantGroups {
                        participantGroup.activeParticipantCount -= 1
                    }
                    
                    //Make sure that the participant we're trying to remove is contained in our array
                    if let index = self.participants.indexOf(participantToRemove!) {
                        self.participants.removeAtIndex(index)
                        
                        //Check if we're in the 'People'-Tab
                        if (self.segmentedControl.selectedSegmentIndex == 1) {
                            //Set the correct Datasource
                            self.displayedArray = self.participants
                            
                            //Update the UI
                            self.tableView.beginUpdates()
                            
                            //Remove the corresponding row for the removed peer in the TableView
                            self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
                            
                            self.tableView.endUpdates()
                        }
                        else if (self.segmentedControl.selectedSegmentIndex == 0) {
                            //Refresh rooms
                            var groupsToRemove = [Group]()
                            
                            for group in self.groups {
                                //If we have at least one participant - add the group to the displayed group array
                                if (!self.isGroupActive(group) && group.groupId != "NEARBY-PEOPLE") {
                                    groupsToRemove.append(group)
                                }
                            }
                            
                            for group in groupsToRemove {
                                if let index = self.groups.indexOf(group) {
                                    self.groups.removeAtIndex(index)
                                    
                                    //Set the correct Datasource
                                    self.displayedArray = self.groups
                                    
                                    //Update the UI
                                    self.tableView.beginUpdates()
                                    
                                    //Remove the corresponding row for the removed peer in the TableView
                                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
                                    
                                    self.tableView.endUpdates()
                                }
                            }
                            
                            //Refresh any cells that were affected by the participant (the active participant count will have changed)
                            self.refreshCellsForGroups(participantGroups)
                        }
                    }
                }
            }
            
            //Refresh the nearby people group's participants
            self.nearbyPeopleGroup.participants = self.participants
        })
    }
    
    func messageSent(notification: NSNotification) {
        //Extract data from the notification
        if let userInfo = notification.userInfo {
            //Extract the conversationId that's contained in the userInfo of the notification
            let conversationId = userInfo["conversationId"] as! String
            
            //Search for the corresponding group or participant in the arrays
            for (index, participant) in self.participants.enumerate() {
                //Check if the IDs match
                if participant.meshPeer.meshPeerID == conversationId {
                    //Move (remove & re-insert) the participant to index 0 of the array
                    self.participants.insert(self.participants.removeAtIndex(index), atIndex: 0)
                    //print("rearanging particpant \(participant.meshPeer.displayName) to top")
                    return
                }
            }
            
            //Groups are stored in the DataManager so we have to iterate through this array and resort it
            for (index, group) in DataManager.sharedInstance.groups.enumerate() {
                //Check if the IDs match
                if group.groupId == conversationId {
                    //Move (remove & re-insert) the group to index 0 of the array
                    DataManager.sharedInstance.groups.insert(DataManager.sharedInstance.groups.removeAtIndex(index), atIndex: 0)
                    self.refreshActiveGroups()
                    //print("rearanging group \(group.title) to top")
                    return
                }
            }
        }
    }
    
    func messageReceived(notification: NSNotification) {
        //Extract data from the notification
        if let userInfo = notification.userInfo {
            //Extract the message that's contained in the userInfo of the notification
            let message = userInfo["message"] as! Message
            
            //Make sure that we're operating on the UI-thread
            dispatch_async(dispatch_get_main_queue()) {
                //Safely unpack the information that's contained inside the message-object
                if let messagePayloadDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(message.payload)! as? NSDictionary {
                    if let text = String(data: messagePayloadDictionary[Constants.Message.Key.Payload] as! NSData, encoding: NSUTF8StringEncoding) {
                        var groupToMove: Group?
                        var moveFromIndex: Int?
                        
                        //Iterate all groups and find the group that this message was received for
                        for (index, group) in self.groups.enumerate() {
                            if (message.conversationId == group.groupId) {
                                group.infoText = text
                                
                                groupToMove = group
                                moveFromIndex = index
                                
                                //Update the DataManager
                                if let dataManagerIndex = DataManager.sharedInstance.groups.indexOf(group) {
                                    DataManager.sharedInstance.groups.removeAtIndex(dataManagerIndex)
                                    DataManager.sharedInstance.groups.insert(group, atIndex: 0)
                                }
                                
                                break
                            }
                        }
   
                        //Check the currently selected tab
                        if (self.segmentedControl.selectedSegmentIndex == 0) {
                            //Currently groups are selected

                            //Move the group to the top as it is the group that has the most recent events
                            if groupToMove != nil {
                                self.moveGroupToTop(groupToMove!, fromIndex: moveFromIndex!)
                            }
                        }
                        else {
                            //Currently people are selected
                            for (index, participant) in self.participants.enumerate() {
                                if (participant.meshPeer.meshPeerID == message.conversationId) {
                                    participant.infoText = text
                                    
                                    //Move the participant to the top as it is the one that has the most recent events
                                    self.moveParticipantToTop(participant, fromIndex: index)
                                    
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func invitationReceived() {
        //Refresh the data source and update the UI
        dispatch_async(dispatch_get_main_queue()) { 
            self.refreshActiveGroupsAndReloadUI()
        }
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayedArray.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //Instantiate our custom cell from the storyboard
        let cell = tableView.dequeueReusableCellWithIdentifier("OverViewCell") as! OverViewCell
        
        //Define variables that are assigned later and set to UI-components of the cell
        let title: String!
        let detailInformation: String!
        var timeLabelText = ""
        
        //Set the cell as read (might be set as unread later)
        self.displayCell(cell, asRead: true)
        
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            //Extract the corresponding group for this cell
            let group = self.displayedArray[indexPath.row] as! Group
            
            //Check for storedMessages
            if (DataManager.sharedInstance.storedMessages[group.groupId] != nil) {
                //We have storedMessages for this conversation - load the last one and display it
                if let storedMessage = DataManager.sharedInstance.storedMessages[group.groupId]!.last {
                    //Set the timeLabel text
                    timeLabelText = self.getFormattedDate(storedMessage.timeStamp)
                    
                    //Extract the content of the stored message
                    if let text = String(data: storedMessage.payload, encoding: NSUTF8StringEncoding) {
                        if (storedMessage.senderId == meshFramework.meshPeer!.meshPeerID) {
                            //We sent the last message - add a "You:"
                            group.infoText = "You: " + text
                            
                            self.displayCell(cell, asRead: true)
                        }
                        else {
                            //The conversation partner sent the message and not us
                            group.infoText = storedMessage.senderDisplayName + ": " + text
                            
                            //Set the cell as read depending on the entry of readConversations in the DataManager
                            if let conversationRead = DataManager.sharedInstance.readConversations[storedMessage.conversationId] {
                                self.displayCell(cell, asRead: conversationRead)
                            }
                        }
                    }
                }
            }
            
            //Display Groups
            title = group.title
            detailInformation = group.infoText
            
            //Display the number of participants in the UI
            cell.nrParticipants.hidden = false

            //Specifically handle the nearbyPeopleGroup differently from others
            if (group.groupId == "NEARBY-PEOPLE") {
                //Set the groups active participant count equal to the count of participants we have, plus myself
                cell.nrParticipants.textLabel.text = String(self.participants.count + 1)
                cell.avatarImageView.backgroundColor = group.color
                cell.avatarImageView.textLabel.text = group.initials
            }
            else {
                //We're dealing with "normal" groups
                cell.nrParticipants.textLabel.text = String(group.activeParticipantCount)
                cell.avatarImageView.backgroundColor = group.color
                cell.avatarImageView.textLabel.text = group.initials
            }
        }
        else {
            //Display people
            let participant = self.displayedArray[indexPath.row] as! Participant
            
            //Check for storedMessages
            if (DataManager.sharedInstance.storedMessages[participant.meshPeer.meshPeerID] != nil) {
                //We have storedMessages for this conversation - load the last one and display it
                if let storedMessage = DataManager.sharedInstance.storedMessages[participant.meshPeer.meshPeerID]!.last {
                    //Set the timeLabel text
                    timeLabelText = self.getFormattedDate(storedMessage.timeStamp)
                    
                    //Extract the content of the stored message
                    if let text = String(data: storedMessage.payload, encoding: NSUTF8StringEncoding) {
                        if (storedMessage.senderId == meshFramework.meshPeer!.meshPeerID) {
                            //We sent the last message - add a "You:"
                            participant.infoText = "You: " + text
                            
                            self.displayCell(cell, asRead: true)
                        }
                        else {
                            //The conversation partner sent the message and not us
                            participant.infoText = text
                            
                            //Set the cell as read depending on the entry of readConversations in the DataManager
                            if let conversationRead = DataManager.sharedInstance.readConversations[storedMessage.conversationId] {
                                self.displayCell(cell, asRead: conversationRead)
                            }
                        }
                    }
                }
            }
            else {
                //Display the status if there were no previous messages
                participant.infoText = "Let's start a conversation"
            }
            
            title = participant.meshPeer.displayName
            detailInformation = participant.infoText
            
            //Hide the number of participants, as they're not needed for individual chats
            cell.nrParticipants.hidden = true
            cell.avatarImageView.backgroundColor = participant.color
            cell.avatarImageView.textLabel.text = participant.initials
        }
        
        //Set the previously defined values for the cell
        cell.title.text = title.uppercaseString
        cell.detailInformation.text = detailInformation
        cell.avatarImageView.textLabel.font = UIFont.systemFontOfSize(20.0)
        cell.timeLabel.text = timeLabelText

        return cell
    }
    
    private func displayCell(cell: OverViewCell, asRead read: Bool) {
        //Set the labels bold if the message has not been read yet
        if (read) {
            cell.title.font = UIFont.systemFontOfSize(15.0)
            cell.detailInformation.font = UIFont.systemFontOfSize(15.0)
            cell.detailInformation.textColor = UIColor.lightGrayColor()
            cell.timeLabel.font = UIFont.systemFontOfSize(13.0)
            cell.timeLabel.textColor = UIColor.lightGrayColor()
        }
        else {
            cell.title.font = UIFont.boldSystemFontOfSize(15.0)
            cell.detailInformation.font = UIFont.boldSystemFontOfSize(15.0)
            cell.detailInformation.textColor = UIColor.blackColor()
            cell.timeLabel.font = UIFont.boldSystemFontOfSize(13.0)
            cell.timeLabel.textColor = UIColor.blackColor()
        }
    }
    
    private func moveGroupToTop(group: Group, fromIndex index: Int) {
        //Handle the nearbyPeopleGroup differently from others
        if (group.groupId != "NEARBY-PEOPLE") {
            //Reload the row that is about to be moved
            self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
            
            //Put this conversation on top
            self.groups.removeAtIndex(index)
            self.groups.insert(group, atIndex: 1)
            
            //Set the correct data source
            self.displayedArray = self.groups
            
            //Move the group below the nearbyPeopleGroup (which is index 1, the nearbyPeopleGroup should always be on top)
            self.tableView.moveRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), toIndexPath: NSIndexPath(forRow: 1, inSection: 0))
        }
        else {
            //Just reload the row
            self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
        }
    }
    
    private func moveParticipantToTop(participant: Participant, fromIndex index: Int) {
        //Reload the row that is about to be moved
        self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
        
        //Put this conversation on top
        self.participants.removeAtIndex(index)
        self.participants.insert(participant, atIndex: 0)
        
        //Set the correct data source
        self.displayedArray = self.participants
        
        //Move the participant to the top (index 0)
        self.tableView.moveRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (sender is UITableViewCell) {
            //Get hold of the cell that caused the segue to be triggered
            let cell = sender as! UITableViewCell
            
            if (self.segmentedControl.selectedSegmentIndex == 0) {
                //Groups
                if let indexPath = tableView.indexPathForCell(cell) {
                    //Identify the selected group
                    let selectedGroup = self.groups[indexPath.row]
                    
                    //Pass over values needed to the destinationViewController
                    let chatViewController = segue.destinationViewController as! ChatViewController
                    chatViewController.conversationPartners = selectedGroup.participants
                    chatViewController.activeParticipants = participants
                    chatViewController.group = selectedGroup
                }
            }
            else {
                //Chat with one individual
                if let indexPath = tableView.indexPathForCell(cell) {
                    let selectedPeer = self.participants[indexPath.row]
                    
                    //Pass over values needed to the destinationViewController
                    let chatViewController = segue.destinationViewController as! ChatViewController
                    chatViewController.conversationPartners = [selectedPeer]
                    chatViewController.activeParticipants = participants
                }
            }
        }
        else if (sender is UIBarButtonItem) {
            //The "+"-Button was clicked
            let destinationViewController = segue.destinationViewController as! CreateGroupViewController
            
            //Pass over the participants to the destinationViewController
            destinationViewController.participants = self.participants
        }
    }
}