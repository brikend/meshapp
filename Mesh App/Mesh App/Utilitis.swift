//
//  Utilitis.swift
//  Mesh App
//
//  Created by Student on 16.05.2016.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation

class Utilities {
    
    static func createInitials(fullName: String) -> String {
        //Split the given fullName by any occurance of spaces
        let fullNameArray = fullName.componentsSeparatedByString(" ")
        
        if (fullNameArray.count == 1) {
            //We have only one word
            if (fullNameArray[0].characters.count == 1) {
                //Only one character
                return fullNameArray[0].uppercaseString
            }
            else if (fullNameArray[0].characters.count > 1) {
                //There are 2 or more characters
                let firstWord = fullNameArray[0]
                
                //Concatenate the first and the second character of the word
                let firstCharacter = String(firstWord[firstWord.startIndex]).uppercaseString
                let secondCharacter = String(firstWord[firstWord.startIndex.successor()]).uppercaseString
                
                return firstCharacter + secondCharacter
            }
        }
        else {
            //We have multiple words
            let firstWord = fullNameArray[0]
            let firstCharacter = String(firstWord[firstWord.startIndex]).uppercaseString
            
            let secondWord = fullNameArray[1]
            let secondCharacter = String(secondWord[secondWord.startIndex]).uppercaseString
            
            //Concatenate the first character of each of the words
            return firstCharacter + secondCharacter
        }
        
        return ""
    }
}