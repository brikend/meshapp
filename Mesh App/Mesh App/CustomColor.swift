//
//  CustomColor.swift
//  Mesh App
//
//  Created by Manuel Schwandt on 24/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import UIKit

//Extension of the existing UIColor-Class that adds custom functions for later use
extension UIColor {
    class func darkMeshChatColor() -> UIColor {
        return UIColor(red: 12.0/255.0, green: 136.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    }
    
    class func lightMeshChatColor() -> UIColor {
        return UIColor(red: 54.0/255, green: 164.0/255, blue: 93.0/255, alpha: 1)
    }
    
    class func randomColor() -> UIColor {
        let avatarColors: NSArray = [
            UIColor(red: 47.0/255.0, green: 112.0/255.0, blue: 225.0/255.0, alpha: 1.0),
            UIColor(red: 83.0/255.0, green: 215.0/255.0, blue: 106.0/255.0, alpha: 1.0),
            UIColor(red: 229.0/255.0, green: 0.0/255.0, blue: 15.0/255.0, alpha: 1.0),
            UIColor(red: 103.0/255.0, green: 153.0/255.0, blue: 170.0/255.0, alpha: 1.0),
            UIColor(red: 0.0/255.0, green: 178.0/255.0, blue: 238.0/255.0, alpha: 1.0),
            UIColor(red: 23.0/255.0, green: 137.0/255.0, blue: 155.0/255.0, alpha: 1.0),
            UIColor(red: 99.0/255.0, green: 214.0/255.0, blue: 74.0/255.0, alpha: 1.0),
            UIColor(red: 134.0/255.0, green: 198.0/255.0, blue: 124.0/255.0, alpha: 1.0),
            UIColor(red: 233.0/255.0, green: 87.0/255.0, blue: 95.0/255.0, alpha: 1.0),
            UIColor(red: 242.0/255.0, green: 71.0/255.0, blue: 63.0/255.0, alpha: 1.0),
            UIColor(red: 140.0/255.0, green: 93.0/255.0, blue: 228.0/255.0, alpha: 1.0),
            UIColor(red: 229.0/255.0, green: 227.0/255.0, blue: 58.0/255.0, alpha: 1.0),
            UIColor(red: 247.0/255.0, green: 145.0/255.0, blue: 55.0/255.0, alpha: 1.0),
            UIColor(red: 54.0/255.0, green: 11.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        ]
        
        let index = Int(arc4random_uniform(UInt32(avatarColors.count - 1)))
        
        return avatarColors[index] as! UIColor
    }
}