//
//  ChatViewController.swift
//  Mesh App
//
//  Created by Student on 21.04.2016.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import JSQMessagesViewController
import MeshBroadcastFramework

//A ViewController that handles chat conversations (incoming and outcoming messages) between the participating people. Refer to http://www.jessesquires.com/JSQMessagesViewController/ or https://github.com/jessesquires/JSQMessagesViewController for a detailed explanation
class ChatViewController: JSQMessagesViewController {
    //Variables related to the chat itself
    var group: Group?
    var conversationPartners: [Participant]!
    var activeParticipants: [Participant]!
    var conversationId: String!
    var messages = [JSQMessage]()
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    //Outlet-variables that are linked to the Storyboard
    @IBOutlet weak var listButton: UIBarButtonItem!
    
    private var localTyping = false
    
    //Get the meshframework-instance from the AppDelegate
    private let meshFramework = (UIApplication.sharedApplication().delegate as! AppDelegate).meshFramework
    
    //Keep track of whether the user is typing on the keyboard or not
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the conversation id
        //Check if it's a group
        if (self.group == nil) {
            //It's not a group chat
            if let conversationPartner = self.conversationPartners.first {
                conversationId = conversationPartner.meshPeer.meshPeerID
            }
            
            //Disable the "List"-Button when the conversation is not a group conversation
            self.listButton.enabled = false
            self.listButton.tintColor = UIColor.clearColor()
        }
        else {
            //It's a group chat
            conversationId = self.group!.groupId
            
            //Enable the "List"-Button when the conversation is a group conversation
            self.listButton.enabled = true
            self.listButton.tintColor = UIColor.whiteColor()
        }
        
        //Set the last message of this conversation to read
        DataManager.sharedInstance.readConversations[conversationId] = true
        
        //Register a method for an incoming notification about received messages
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.messageReceived(_:)), name: "messageReceivedNotification", object: nil)

        if (self.group == nil) {
            //It's an individual chat with one other peer
            self.title = conversationPartners?.first?.meshPeer.displayName
        }
        else {
            //It's a group chat
            self.title = self.group!.title
        }
        
        self.senderId = self.meshFramework.meshPeer!.meshPeerID
        self.senderDisplayName = self.meshFramework.meshPeer!.displayName
        
        setupBubbles()
        
        //No avatars
        //collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSizeZero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero
        
        self.navigationController!.view.backgroundColor = UIColor.whiteColor()
        
        //Set the color of the send button
        self.inputToolbar.contentView.rightBarButtonItem.setTitleColor(UIColor.lightMeshChatColor(), forState: UIControlState.Normal)
        self.inputToolbar.contentView.rightBarButtonItem.setTitleColor(UIColor.lightMeshChatColor().colorWithAlphaComponent(0.5), forState: UIControlState.Highlighted)
        
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.collectionView.backgroundView = backgroundImageView
        
        self.collectionView.contentOffset = CGPointMake(0, self.navigationController!.navigationBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.height)
        
        self.loadMessages()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setBackgroundImage()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        observeTyping()
        //self.collectionView.collectionViewLayout.springinessEnabled = true;
        
        //NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: #selector(ChatViewController.sendTestMessage), userInfo: nil, repeats: true)
    }
    
    var index = 1
    
    func sendTestMessage() {
        self.didPressSendButton(UIButton(), withMessageText: "\(index)", senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: NSDate())
        index += 1
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.setBackgroundImage()
    }
    
    func setBackgroundImage() {
        let app = UIApplication.sharedApplication()
        
        let backgroundImage: UIImage!
        
        if (app.statusBarOrientation.isLandscape) {
            // Image Code
            backgroundImage = UIImage(named: "background-landscape")
        }
        else {
            // Image Code
            backgroundImage = UIImage(named: "background")
        }
        
        let backgroundImageView = self.collectionView.backgroundView as! UIImageView
        backgroundImageView.image = backgroundImage
    }

    
    //MARK: - CollectionViewDataSource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
            as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView!.textColor = UIColor.whiteColor()
        } else {
            cell.textView!.textColor = UIColor.blackColor()
        }
        
        return cell
    }
    
    
    //MARK: - Message configuration
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        
        if (message.senderId == senderId) {
            return outgoingBubbleImageView
        }
        else {
            return incomingBubbleImageView
        }
    }
    
    //MARK: - Avatars
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let message = messages[indexPath.item]
        
        var initials: String?
        var color: UIColor?
        
        for participant in self.conversationPartners {
            print("message sender: \(message.senderId)")
            print("participant id: \(participant.meshPeer.meshPeerID)")
            if (message.senderId == participant.meshPeer.meshPeerID) {
                initials = participant.initials
                color = participant.color
            }
        }
        
        if (initials != nil && color != nil) {
            return JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(initials!, backgroundColor: color!, textColor: UIColor.whiteColor(), font: UIFont.systemFontOfSize(14.0), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        }
        else {
            return JSQMessagesAvatarImageFactory.avatarImageWithUserInitials("ER", backgroundColor: UIColor.redColor(), textColor: UIColor.whiteColor(), font: UIFont.systemFontOfSize(14.0), diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
        }
    }
    
    func addMessage(id: String, displayName: String, text: String) {
        let message = JSQMessage(senderId: id, displayName: displayName, text: text)
        messages.append(message)
    }
 
    
    //MARK: - Usernames
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let data = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        if (self.senderDisplayName == data.senderDisplayName()) {
            return nil
        }
        return NSAttributedString(string: data.senderDisplayName())
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        let data = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        if (self.senderDisplayName == data.senderDisplayName()) {
            return 0.0
        }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    //MARK: Timestamp
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let currentMessage = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        
        if (indexPath.row == 0) {
            return JSQMessagesTimestampFormatter.sharedFormatter().attributedTimestampForDate(currentMessage.date())
        }
        
        if (indexPath.row > 0) {
            //Display a timestamp if the previous message was received more than 1 minute ago
            if let previousMessage = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: NSIndexPath(forRow: indexPath.row - 1, inSection: 0)) {
                if (currentMessage.date().timeIntervalSinceDate(previousMessage.date()) >= 60) {
                    return JSQMessagesTimestampFormatter.sharedFormatter().attributedTimestampForDate(currentMessage.date())
                }
            }
        }
        
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        let currentMessage = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: indexPath)
        
        if (indexPath.row == 0) {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        if (indexPath.row > 0) {
            //Display a timestamp if the previous message was received more than 1 minute ago
            if let previousMessage = self.collectionView(self.collectionView, messageDataForItemAtIndexPath: NSIndexPath(forRow: indexPath.row - 1, inSection: 0)) {
                if (currentMessage.date().timeIntervalSinceDate(previousMessage.date()) >= 60) {
                    return kJSQMessagesCollectionViewCellLabelHeightDefault
                }
            }
        }
        
        return 0.0
    }
    
    //MARK: - Keyboard
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        addMessage(senderId, displayName: senderDisplayName, text: text)
        self.finishSendingMessage()
        
        if let textData = text.dataUsingEncoding(NSUTF8StringEncoding) {
            
            let messageDataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Message, Constants.Message.Key.Payload: textData]
            let messageData = NSKeyedArchiver.archivedDataWithRootObject(messageDataDictionary)
            
            if (self.group == nil) {
                //It's not a group chat
                //Send the message and set the conversationId to myself
                meshFramework.sendData(messageData, toPeers: [(self.conversationPartners.first!.meshPeer)], conversationId: self.senderId)
            }
            else {
                //It' a group chat
                if (self.group?.groupId == "NEARBY-PEOPLE") {
                    //Send the message and set the conversationId to the groups conversationId. toPeers has to be nil because it's a broadcast for the nearby-people group
                    self.meshFramework.sendData(messageData, toPeers: nil, conversationId: self.conversationId)
                }
                else {
                    //Send the message and set the conversationId to the groups conversationId
                    //Create an array of meshPeers
                    var meshPeers = [MeshPeer]()
                    for participant in self.conversationPartners {
                        if (self.meshFramework.meshPeer!.meshPeerID != participant.meshPeer.meshPeerID) {
                            meshPeers.append(participant.meshPeer)
                        }
                    }

                    self.meshFramework.sendData(messageData, toPeers: meshPeers, conversationId: self.conversationId)
                }
            }
            
            //Store the message
            DataManager.sharedInstance.storeMessage(MeshAppMessage(conversationId: self.conversationId, senderId: senderId, senderDisplayName: senderDisplayName, messageType: Constants.DataType.Text, payload: textData, timeStamp: NSDate()))
            
            NSNotificationCenter.defaultCenter().postNotificationName("messageSentNotification", object: nil, userInfo: ["conversationId": self.conversationId])
        }
    }
    
    override func textViewDidChange(textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        isTyping = textView.text != ""
    }
    
    private func observeTyping() {
        //let typingIndicatorRef = rootRef.childByAppendingPath("typingIndicator")
        //userIsTypingRef = typingIndicatorRef.childByAppendingPath(senderId)
        //userIsTypingRef.onDisconnectRemoveValue()
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        //Accessory Button pressed
    }
    
    //MARK: - Helper
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory.outgoingMessagesBubbleImageWithColor(UIColor.lightMeshChatColor())
        incomingBubbleImageView = factory.incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
    }
    
    private func loadMessages() {
        let storedMessages: [MeshAppMessage]
        
        if DataManager.sharedInstance.storedMessages[self.conversationId] != nil {
            storedMessages = DataManager.sharedInstance.storedMessages[self.conversationId]!
            
            for storedMessage in storedMessages {
                if let text = String(data: storedMessage.payload, encoding: NSUTF8StringEncoding) {
                    self.addMessage(storedMessage.senderId, displayName: storedMessage.senderDisplayName, text: text)
                }
            }
        }
    }
        
    //MARK: - Mesh Notification functions
    func messageReceived(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let message = userInfo["message"] as! Message
            
            //Check if the conversationId matches the current conversationId
            if (message.conversationId == self.conversationId) {
                //print("ChatViewController message received from: \(message.sender)")
                
                //Set message as read
                DataManager.sharedInstance.readConversations[conversationId] = true
                
                dispatch_async(dispatch_get_main_queue()) {
                    if let messagePayloadDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(message.payload)! as? NSDictionary {
                        if let text = String(data: messagePayloadDictionary[Constants.Message.Key.Payload] as! NSData, encoding: NSUTF8StringEncoding) {
                            JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
                            self.addMessage(message.sender.meshPeerID, displayName: message.sender.displayName, text: text)
                            self.finishReceivingMessage()
                        }
                    }
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "Group Participants View") {
            let groupParticipantsViewController = segue.destinationViewController as! GrouParticipantsViewController
            groupParticipantsViewController.group = self.group
            groupParticipantsViewController.activeParticipants = activeParticipants
            
        }
    }
}