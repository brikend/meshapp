//
//  GrouParticipantsViewController.swift
//  Mesh App
//
//  Created by Student on 18.05.2016.
//  Copyright © 2016 NTNU. All rights reserved.
//

import UIKit

class GrouParticipantsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var group: Group!
    var activeParticipants: [Participant]!
    
    @IBOutlet weak var plusButton: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    
    private let meshFramework = (UIApplication.sharedApplication().delegate as! AppDelegate).meshFramework
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableview.backgroundColor = UIColor.clearColor()
        self.tableview.contentInset = UIEdgeInsetsMake(85.0, 0, 85.0, 0)
        
        //Check if we are in the Nearby Group
        if (group.groupId == "NEARBY-PEOPLE") {
            self.tableview.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
            self.plusButton.hidden = true
            self.plusButton.userInteractionEnabled = false
        }
        
        
        //Round the plus image on the view
        plusButton.backgroundColor = UIColor.lightMeshChatColor()
        plusButton.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        plusButton.layer.cornerRadius = plusButton.bounds.size.width/2
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GrouParticipantsViewController.plusImageTapped))
        plusButton.userInteractionEnabled = true
        plusButton.addGestureRecognizer(tapGestureRecognizer)
        
        
        //Background image for the table view
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.tableview.backgroundView = backgroundImageView
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setBackgroundImage()
    }
    
    override func viewDidAppear(animated: Bool) {
        if let index = DataManager.sharedInstance.groups.indexOf(self.group){
            self.group.participants = DataManager.sharedInstance.groups[index].participants
        }
        
        tableview.reloadData()
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.setBackgroundImage()
    }
    
    func setBackgroundImage() {
        let app = UIApplication.sharedApplication()
        
        let backgroundImage: UIImage!
        
        if (app.statusBarOrientation.isLandscape) {
            // Image Code
            backgroundImage = UIImage(named: "background-landscape")
        }
        else {
            // Image Code
            backgroundImage = UIImage(named: "background")
        }
        
        let backgroundImageView = self.tableview.backgroundView as! UIImageView
        backgroundImageView.image = backgroundImage
    }
    
    func plusImageTapped() {
        performSegueWithIdentifier("NewParticipantInGroup View", sender: nil)
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.group.participants.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectGroupParticipantCell") as! SelectGroupParticipantCell
        let participant = self.group.participants[indexPath.row]
        
        if(participant.meshPeer.meshPeerID == meshFramework.meshPeer?.meshPeerID){
            cell.participantName.text = "You"
        }
        else {
            cell.participantName.text = self.group.participants[indexPath.row].meshPeer.displayName
        }
        
        cell.avatarInitialView.backgroundColor = participant.color
        cell.avatarInitialView.textLabel.text = participant.initials
        cell.participant = participant
        
        if (!activeParticipants.contains(participant) && participant.meshPeer.meshPeerID != meshFramework.meshPeer?.meshPeerID) {
            cell.participantName.textColor = UIColor.lightGrayColor()
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "NewParticipantInGroup View") {
            let addParticipantToGroupViewController = segue.destinationViewController as! AddParticipantToGroupViewController
            addParticipantToGroupViewController.group = self.group
            addParticipantToGroupViewController.activeParticipants = activeParticipants
        }
    }
}
