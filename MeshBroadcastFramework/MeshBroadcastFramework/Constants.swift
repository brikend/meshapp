//
//  Constants.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 18/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation

struct Constants {
    struct Message {
        struct Key {
            static let MessageType = "MessageType"
            static let Payload = "Payload"
        }
        struct Type {
            static let Message = "Message"
            static let Heartbeat = "Heartbeat"
        }
    }
}