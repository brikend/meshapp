//
//  Message.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation

public class Message: NSObject, NSCoding {
    let id: String
    public let payload: NSData
    public let timeStamp: NSDate
    public let sender: MeshPeer
    public let conversationId: String
    public let receivers: [MeshPeer]?
    
    init(id: String, payload: NSData, timeStamp: NSDate, sender: MeshPeer, conversationId: String, receivers: [MeshPeer]?) {
        self.id = id
        self.payload = payload
        self.timeStamp = timeStamp
        self.sender = sender
        self.conversationId = conversationId
        self.receivers = receivers
    }
    
    //MARK: - NSCoding
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObjectForKey("id") as! String
        self.payload = aDecoder.decodeObjectForKey("payload") as! NSData
        self.timeStamp = aDecoder.decodeObjectForKey("timeStamp") as! NSDate
        self.sender = aDecoder.decodeObjectForKey("sender") as! MeshPeer
        self.conversationId = aDecoder.decodeObjectForKey("conversationId") as! String
        self.receivers = aDecoder.decodeObjectForKey("receivers") as! [MeshPeer]?
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.id, forKey: "id")
        aCoder.encodeObject(self.payload, forKey: "payload")
        aCoder.encodeObject(self.timeStamp, forKey: "timeStamp")
        aCoder.encodeObject(self.sender, forKey: "sender")
        aCoder.encodeObject(self.conversationId, forKey: "conversationId")
        aCoder.encodeObject(self.receivers, forKey: "receivers")
    }
}