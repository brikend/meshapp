//
//  MeshPeer.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MultipeerConnectivity

public class MeshPeer: NSObject, NSCoding {
    public let meshPeerID: String
    public let displayName: String
    public var modified: NSDate
    
    public init(meshPeerID: String, displayName: String, modified: NSDate) {
        self.meshPeerID = meshPeerID
        self.displayName = displayName
        self.modified = modified
    }
    
    //MARK: - NSCoding
    required public init(coder aDecoder: NSCoder) {
        self.meshPeerID = aDecoder.decodeObjectForKey("meshPeerID") as! String
        self.displayName = aDecoder.decodeObjectForKey("displayName") as! String
        self.modified = aDecoder.decodeObjectForKey("modified") as! NSDate
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.meshPeerID, forKey: "meshPeerID")
        aCoder.encodeObject(self.displayName, forKey: "displayName")
        aCoder.encodeObject(self.modified, forKey: "modified")
    }
}