import Foundation

public class Heartbeat: NSObject, NSCoding {
    let id: String
    public let sender: MeshPeer
    
    init(id: String, sender: MeshPeer) {
        self.id = id
        self.sender = sender
    }
    
    //MARK: - NSCoding
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObjectForKey("id") as! String
        self.sender = aDecoder.decodeObjectForKey("sender") as! MeshPeer
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.id, forKey: "id")
        aCoder.encodeObject(self.sender, forKey: "sender")
    }
}