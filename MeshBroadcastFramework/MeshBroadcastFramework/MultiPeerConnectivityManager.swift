//
//  MultiPeerConnectivityManager.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class MultiPeerConnectivityManager: NSObject, MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate {
    //TODO: Implement this to be dynamic
    //Init peerID
    let mcPeerID = MCPeerID(displayName: UIDevice.currentDevice().name)
    
    //TODO: Implement this to be dynamic
    //Init meshPeerID
    let meshPeerID = UIDevice.currentDevice().identifierForVendor?.UUIDString
    var peerIDMapping = [String: MCPeerID]()
    
    //Init serviceType
    private let serviceType = "ntnu-meshapp"
    
    //Define advertiser
    private var serviceAdvertiser: MCNearbyServiceAdvertiser!
    
    //Define browser
    private var serviceBrowser: MCNearbyServiceBrowser!
    
    private var sessions = [MCSession]()
    
    private var invitations = [String]()
    
    private let meshManager: MeshManager
    
    private let lockQueue = dispatch_queue_create("no.ntnu.lockQueue", nil)
    
    init(meshManager: MeshManager) {
        self.meshManager = meshManager
        
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: self.mcPeerID, discoveryInfo: ["meshPeerID": self.meshPeerID!], serviceType: self.serviceType)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: self.mcPeerID, serviceType: self.serviceType)
        
        super.init()
        
        //Set the delegates
        self.serviceAdvertiser.delegate = self
        self.serviceBrowser.delegate = self
        
        //Start advertising and browsing
        self.serviceAdvertiser.startAdvertisingPeer()
        self.serviceBrowser.startBrowsingForPeers()
        
        //Register functions that should be called for incoming notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MultiPeerConnectivityManager.stopBrowsingAndAdvertising), name: "applicationDidEnterBackgroundNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MultiPeerConnectivityManager.wakeup), name: "applicationWillEnterForegroundNotification", object: nil)
    }
    
    deinit {
        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, forKeyPath: "applicationDidEnterBackgroundNotification")
        NSNotificationCenter.defaultCenter().removeObserver(self, forKeyPath: "applicationWillEnterForegroundNotification")
    }
    
    func reinstantiateBrowserAndAdvertiser() {
        self.serviceAdvertiser = nil
        self.serviceBrowser = nil
        
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: self.mcPeerID, discoveryInfo: ["meshPeerID": self.meshPeerID!], serviceType: self.serviceType)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: self.mcPeerID, serviceType: self.serviceType)
        
        //Set the delegates
        self.serviceAdvertiser.delegate = self
        self.serviceBrowser.delegate = self
        
        //Start advertising and browsing
        self.serviceAdvertiser.startAdvertisingPeer()
        self.serviceBrowser.startBrowsingForPeers()
    }
    
    func sendDataToAllNearbyPeers(data: NSData, excludingPeer: MCPeerID?) {
        //Send data to all stored sessions (one peer per session)
        for session in self.sessions {
            if (excludingPeer != nil) {
                if (session.connectedPeers.contains(excludingPeer!)) {
                    //Do not send a message to the excluded peer
                    continue
                }
            }

            if (session.connectedPeers.count > 0) {
                do {
                    try session.sendData(data, toPeers: session.connectedPeers, withMode: MCSessionSendDataMode.Reliable)
                }
                catch let error as NSError {
                    print("sendDataToAllNearbyPeers - Error: \(error)")
                }
            }
        }
    }
    
    func disconnectFromPeer(meshPeer: MeshPeer) {        
        var sessionsToCleanUp = [MCSession]()
        
        //Disconnect form the session that containts the peer
        for session in self.sessions {
            if (session.connectedPeers.count == 0) {
                sessionsToCleanUp.append(session)
            }
            else {
                //Find the corresponding mcPeer in the mapping
                if let peerID = self.peerIDMapping[meshPeer.meshPeerID] {
                    if session.connectedPeers.contains(peerID) {
                        sessionsToCleanUp.append(session)
                    }
                }
            }
        }
        
        for session in sessionsToCleanUp {
            self.cleanupSession(session)
        }
        
        //Remove the peerID in the invitations if it's already existing
        if (self.invitations.contains(meshPeer.meshPeerID)) {
            self.invitations.removeAtIndex(self.invitations.indexOf(meshPeer.meshPeerID)!)
        }
        
        self.peerIDMapping.removeValueForKey(meshPeer.meshPeerID)
    }
    
    //MARK: - Wakeup
    func wakeup() {
        self.reinstantiateBrowserAndAdvertiser()
    }
    
    //MARK: - Cleanup
    func stopBrowsingAndAdvertising() {
        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
    }
    
    func cleanupSession(session: MCSession) {
        //Disconnect the session and remove it from the array
        session.disconnect()
        
        if let sessionIndex = self.sessions.indexOf(session) {
            self.sessions.removeAtIndex(sessionIndex)
        }
        
        self.reinstantiateBrowserAndAdvertiser()
    }
    
    //MARK: MCNearbyServiceAdvertiserDelegate
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: NSError) {
    }
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: NSData?, invitationHandler: (Bool, MCSession) -> Void) {
        dispatch_sync(lockQueue) {
            let session = MCSession(peer: self.mcPeerID, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.None)
            
            if (context != nil) {
                if let providedMeshPeer = NSKeyedUnarchiver.unarchiveObjectWithData(context!) as? String {
                    self.invitations.append(providedMeshPeer)
                    
                    let session = MCSession(peer: self.mcPeerID, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.None)
                    session.delegate = self
                    self.sessions.append(session)
                    
                    print("didReceiveInvitationFromPeer")
                    
                    invitationHandler(true, session)
                    
                    return
                }
            }
            
            invitationHandler(false, session)
        }
    }
    
    //MARK: MCNearbyServiceBrowserDelegate
    func browser(browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        //print("lostPeer - Lost peer \(peerID.displayName)")
    }
    
    func browser(browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: NSError) {
        //print("didNotStartBrowsingForPeers - Error \(error)")
    }
    
    func browser(browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        dispatch_sync(lockQueue) {
            print("foundPeer - Found peer \(peerID.displayName)")
            
            //Extract the meshPeerID
            if (info != nil) {
                if let providedMeshPeerID = info!["meshPeerID"] {
                    //Add the peer to the peerIDMapping dictionary
                    self.peerIDMapping[providedMeshPeerID] = peerID
                    
                    if (providedMeshPeerID > self.meshPeerID) {
                        if (!self.invitations.contains(providedMeshPeerID)) {
                            print("Inviting peer \(peerID.displayName)")
                            
                            self.invitations.append(providedMeshPeerID)
                            
                            let session = MCSession(peer: self.mcPeerID, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.None)
                            session.delegate = self
                            self.sessions.append(session)
                            
                            browser.invitePeer(peerID, toSession: session, withContext: NSKeyedArchiver.archivedDataWithRootObject(self.meshPeerID!), timeout: 30)
                        }
                    }
                    else {
                        print("Waiting for invitation from \(peerID.displayName)")
                    }
                }
            }
        }
    }
    
    //MARK: MCSessionDelegate
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        //Forward it to the MeshManager
        self.meshManager.didReceiveData(data, fromPeer: peerID)
    }
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
    }
    
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        //print("didReceiveStream")
    }
    
    func session(session: MCSession, didReceiveCertificate certificate: [AnyObject]?, fromPeer peerID: MCPeerID, certificateHandler: (Bool) -> Void) {
        //print("didReceiveCertificate")
        certificateHandler(true)
    }
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        //print("didStartReceivingResourceWithName")
    }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        //print("didFinishReceivingResourceWithName")
    }
}