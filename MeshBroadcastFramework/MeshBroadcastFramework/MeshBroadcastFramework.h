//
//  MeshBroadcastFramework.h
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MeshBroadcastFramework.
FOUNDATION_EXPORT double MeshBroadcastFrameworkVersionNumber;

//! Project version string for MeshBroadcastFramework.
FOUNDATION_EXPORT const unsigned char MeshBroadcastFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MeshBroadcastFramework/PublicHeader.h>

