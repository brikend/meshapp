//
//  MeshManager.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class MeshManager: NSObject {
    //Private properties
    var myMeshPeer: MeshPeer!
    private var peerList = [MeshPeer]()
    private var multiPeerConnectivityManager: MultiPeerConnectivityManager!
    var delegate: MeshBroadcastFrameworkDelegate?
    
    //Timers for recurring actions
    private var sendHeartbeatTimer: NSTimer!
    private var updatePeerListTimer: NSTimer!
    private var cleanupMessagesTimer: NSTimer!
    
    private var processedMessages = [String: NSDate]()
    
    private let lockQueue = dispatch_queue_create("no.ntnu.lockQueue", nil)
        
    override init() {
        super.init()
        
        //Initialize MultiPeerConnectivityManager
        self.multiPeerConnectivityManager = MultiPeerConnectivityManager(meshManager: self)
        
        //Initialize myMeshPeer
        self.myMeshPeer = MeshPeer(meshPeerID: self.multiPeerConnectivityManager.meshPeerID!, displayName: self.multiPeerConnectivityManager.mcPeerID.displayName, modified: NSDate())
        
        //Initialize the timers
        self.sendHeartbeatTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(MeshManager.sendHeartbeat), userInfo: nil, repeats: true)
        self.updatePeerListTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(MeshManager.updatePeerList), userInfo: nil, repeats: true)
        self.cleanupMessagesTimer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: #selector(MeshManager.cleanupMessages), userInfo: nil, repeats: true)
    }
    
    deinit {
        //Make sure to invalidate the timers when the object gets disposed
        self.sendHeartbeatTimer.invalidate()
        self.updatePeerListTimer.invalidate()
        self.cleanupMessagesTimer.invalidate()
    }
    
    func didReceiveData(data: NSData, fromPeer: MCPeerID) {
        dispatch_sync(lockQueue) {
            //Convert the data into a dictionary
            if let dataDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(data)! as? NSDictionary,
                let messageType = dataDictionary[Constants.Message.Key.MessageType] as? String {
                switch messageType {
                case Constants.Message.Type.Message:
                    if let message = dataDictionary[Constants.Message.Key.Payload] as? Message {
                        //Check if I already processed the message
                        if (!self.processedMessages.keys.contains(message.id)) {
                            //print("Not processed message: \(message.id)")
                            
                            //Store the message as processed
                            self.processedMessages[message.id] = NSDate()
                            
                            //Use the received message as a heartbeat from the sender
                            self.processHeartbeat(message.sender)
                            
                            //Check if it's a broadcast
                            if (message.receivers == nil) {
                                //Inform the delegate
                                self.delegate?.messageReceived(message)
                                
                                //It's a broadcast - send the message to all neighbours except the sender
                                self.multiPeerConnectivityManager.sendDataToAllNearbyPeers(data, excludingPeer: fromPeer)
                            }
                            else {
                                //It's not a broadcast - check if I'm one of the receipients
                                var isMessageForMe = false
                                
                                for meshPeer in message.receivers! {
                                    if (meshPeer.meshPeerID == self.myMeshPeer.meshPeerID) {
                                        //I'm one of the receivers
                                        //Inform the delegate
                                        self.delegate?.messageReceived(message)
                                        
                                        isMessageForMe = true
                                        
                                        break
                                    }
                                }
                                
                                //Check if we still have to forward the message
                                if (message.receivers!.count > 1 || (message.receivers!.count == 1 && !isMessageForMe)) {
                                    //The message is not for me and not a broadcast - I have to forward it
                                    self.multiPeerConnectivityManager.sendDataToAllNearbyPeers(data, excludingPeer: fromPeer)
                                }
                            }
                        }
                    }
                    
                case Constants.Message.Type.Heartbeat:
                    if let message = dataDictionary[Constants.Message.Key.Payload] as? Heartbeat {
                        //Check if I already processed the message
                        if (!self.processedMessages.keys.contains(message.id)) {
                            //Store the message as processed
                            self.processedMessages[message.id] = NSDate()
                            
                            self.processHeartbeat(message.sender)
                            
                            //Send the Heartbeat to all others
                            self.multiPeerConnectivityManager.sendDataToAllNearbyPeers(data, excludingPeer: fromPeer)
                        }
                    }
                    
                default:
                    return
                }
            }
        }
    }
    
    //MARK: - Messsage Sending
    func sendData(data: NSData, toPeers peers: [MeshPeer]?, conversationId: String) {
        dispatch_async(dispatch_get_main_queue()) {
            //Pause the timer for sending heartbeats when we're sending messages
            self.sendHeartbeatTimer.invalidate()
            
            //Create an id for the discovery message
            let message = Message(id: NSUUID().UUIDString, payload: data, timeStamp: NSDate(), sender: self.myMeshPeer, conversationId: conversationId, receivers: peers)
            
            //Store the message id in the processed messages
            self.processedMessages[message.id] = NSDate()
            
            //Add the message type in the message header
            let dataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Message, Constants.Message.Key.Payload: message]
            let messageData = NSKeyedArchiver.archivedDataWithRootObject(dataDictionary)
            
            //Send it to all nearby peers
            self.multiPeerConnectivityManager.sendDataToAllNearbyPeers(messageData, excludingPeer: nil)
            
            //Resume the timer after we sent the message
            self.sendHeartbeatTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(MeshManager.sendHeartbeat), userInfo: nil, repeats: true)
        }
    }
    
    func createUnion(firstArray: [MeshPeer], secondArray: [MeshPeer]) -> [MeshPeer] {
        var unionArray = firstArray
        
        for secondArrayPeer in secondArray {
            var isNewElement = true
            for var unionArrayPeer in unionArray {
                if (secondArrayPeer.meshPeerID == unionArrayPeer.meshPeerID) {
                    isNewElement = false

                    if (unionArrayPeer.modified.compare(secondArrayPeer.modified) == .OrderedAscending) {
                        //unionArrayPeer is older (LESS recently) - replace the meshPeer in the unionArray
                        unionArrayPeer = secondArrayPeer
                    }
                }
            }
            
            if (isNewElement) {
                unionArray.append(secondArrayPeer)
            }
        }
        
        return unionArray
    }
    
    //MARK: - Helper functions
    func getMeshPeerWithID(meshPeerID: String) -> MeshPeer? {
        //Iterate all MeshPeer-Objects
        for meshPeer in self.peerList {
            //Check if their IDs match
            if (meshPeer.meshPeerID == meshPeerID) {
                return meshPeer
            }
        }
        
        return nil
    }
    
    func sendHeartbeat() {
        //Create Heartbeat-Message
        let heartbeat = Heartbeat(id: NSUUID().UUIDString, sender: self.myMeshPeer)
        
        //Store the heartbeat as processed
        self.processedMessages[heartbeat.id] = NSDate()
        
        //Add the message type in the message header
        let dataDictionary = [Constants.Message.Key.MessageType: Constants.Message.Type.Heartbeat, Constants.Message.Key.Payload: heartbeat]
        let messageData = NSKeyedArchiver.archivedDataWithRootObject(dataDictionary)
        
        //Send it to all nearby peers
        self.multiPeerConnectivityManager.sendDataToAllNearbyPeers(messageData, excludingPeer: nil)
    }
    
    func updatePeerList() {
        //Define an array of MeshPeers containing the objects that should be removed
        var peersToRemove = [MeshPeer]()
        
        //Iterate all peers
        for peer in self.peerList {
            //Check if the timestamp of the peer is fresh enough for keeping it
            if (NSDate().timeIntervalSinceDate(peer.modified) >= 10.0) {
                print("Removing - last modified \(NSDate().timeIntervalSinceDate(peer.modified)) ago")
                
                //Append the peer to the array that will be removed
                peersToRemove.append(peer)
            }
            else {
                //print("Not removing - last modified \(NSDate().timeIntervalSinceDate(peer.modified)) ago")
            }
        }
        
        //Remove all of the objects that we acquired in the previous step (by determining if they're too old or not)
        for peerToRemove in peersToRemove {
            if let index = self.peerList.indexOf(peerToRemove) {
                self.peerList.removeAtIndex(index)
                
                //Disconnect from the session
                self.multiPeerConnectivityManager.disconnectFromPeer(peerToRemove)
                
                //Inform the delegate
                self.delegate?.peerDropped(peerToRemove)
            }
        }
    }
    
    func cleanupMessages() {
        print("Cleaning up: there are \(self.processedMessages.count) messages")
        
        //Define an array of Messages containing the objects that should be removed
        var messagesToRemove = [String]()
        
        for (key, value) in self.processedMessages {
            if (NSDate().timeIntervalSinceDate(value) >= 10.0) {
                messagesToRemove.append(key)
            }
        }
        
        for messageToRemove in messagesToRemove {
            self.processedMessages.removeValueForKey(messageToRemove)
        }
        
        print("Finished cleaning up old messages \(messagesToRemove.count)")
    }
    
    func processHeartbeat(sender: MeshPeer) {        
        //Check if the peer is already in our list
        if let meshPeer = self.getMeshPeerWithID(sender.meshPeerID) {
            //If it's already in our list, update the value for "modified"
            meshPeer.modified = NSDate()
        }
        else {
            //If it's not in there, append it now - also, update its 'modified'-value
            sender.modified = NSDate()
            self.peerList.append(sender)
            
            //Inform the delegate about the new peer
            self.delegate?.peerDiscovered(sender)
        }
    }
}