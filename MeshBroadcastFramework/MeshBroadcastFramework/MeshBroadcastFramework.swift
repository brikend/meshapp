//
//  MeshBroadcastFramework.swift
//  MeshBroadcastFramework
//
//  Created by Manuel Schwandt on 17/04/16.
//  Copyright © 2016 NTNU. All rights reserved.
//

import Foundation

public protocol MeshBroadcastFrameworkDelegate {
    func messageReceived(message: Message)
    func peerDiscovered(peer: MeshPeer)
    func peerDropped(peer: MeshPeer)
}

public class MeshBroadcastFramework {
    //Private properties
    private var meshManager: MeshManager!
    
    //Public properties
    public var delegate: MeshBroadcastFrameworkDelegate? {
        didSet {
            self.meshManager?.delegate = self.delegate
        }
    }
    
    //TODO: Implement this to be dynamic
    private(set) public var meshPeer: MeshPeer?
    
    public init() {}
    
    public func startMeshNetwork() {
        print("Starting mesh network")
        
        self.meshManager = MeshManager()
        self.meshManager.delegate = self.delegate
        
        self.meshPeer = meshManager.myMeshPeer
    }
    
    public func stopMeshNetwork() {
        print("Stopping mesh network")
        
        self.meshManager = nil
    }
    
    public func sendData(data: NSData, toPeers peers: [MeshPeer]?, conversationId: String) {
        self.meshManager.sendData(data, toPeers: peers, conversationId: conversationId)
    }
}